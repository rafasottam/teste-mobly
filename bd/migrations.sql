INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (22, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (23, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (24, '2018_07_27_133011_create_products_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (25, '2018_07_27_134117_create_orders_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (26, '2018_07_27_134133_create_order_products_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (27, '2018_07_30_175740_create_category_products_table', 1);
