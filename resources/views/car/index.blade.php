﻿@extends('layout')
@section('pagina_titulo', 'Carrinho' )

@section('pagina_conteudo')

<div class="container">
    <div class="row">
        <h3>Produtos no carrinho</h3>
    </div>
    @forelse ($orders as $order)
        <div class="row">
            <h5 class="col l6 s12 m6"> order: {{ $order->id }} </h5>
            <h5 class="col l6 s12 m6"> Criado em: {{ $order->created_at->format('d/m/Y H:i') }} </h5>
        </div>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Produto</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Valor</th>
                    <th scope="col" colspan="2">#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($order->order_product as $order_product)
                <tr>
                    <th scope="row">{{ $order_product->product_id }}</th>
                    <td><img width="100" height="100" src="{{ $order_product->product->image }}"></td>
                    <td>{{ $order_product->product->name }}</td>
                    <td>R$ {{ number_format($order_product->product->value, 2, ',', '.') }}</td>
                    <td>
                        <form method="POST" action="{{ route('car.remove') }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <input type="hidden" name="order_id" value="{{ $order->id }}">
                            <input type="hidden" name="product_id" value="{{ $order_product->product_id }}">
                            <button type="submit" class="btn btn-danger">x</button> 
                        </form>
                    </td>
                    <td>
                        <form method="POST" action="{{ route('car.add') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $order_product->product_id }}">
                            <button type="submit" class="btn btn-primary">+</button> 
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <a class="btn-large tooltipped col l4 s4 m4 offset-l2 offset-s2 offset-m2" data-position="top" data-delay="50" data-tooltip="Voltar a página inicial para continuar comprando?" href="{{ route('index') }}">Continuar comprando</a>
            <form method="POST" action="{{ route('car.conclude') }}">
                {{ csrf_field() }}
                <input type="hidden" name="order_id" value="{{ $order->id }}">
                <button type="submit" class="btn btn-success">
                    Concluir compra
                </button>   
            </form>
        </div>
    @empty
        <h5>Não há nenhum order no carrinho</h5>
    @endforelse
</div>

@endsection