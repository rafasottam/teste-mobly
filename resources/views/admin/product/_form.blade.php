<div class="form-group">
	<input type="text" name="name" id="nome" required="true" value="{{ isset($product->name) ? $product->name : null }}">
	<label for="nome">Nome</label>
</div>
<div class="form-group">
	<textarea type="text" name="description" id="descricao" required="true">{{ isset($product->description) ? $product->description : null }}</textarea>
	<label for="descricao">Descrição</label>
</div>
<div class="form-group">
	<input type="file" name="image" id="imagem" required="true" value="{{ isset($product->image) ? $product->image : null }}">
</div>
<div class="form-group">
	<input type="text" name="value" id="valor" required="true" value="{{ isset($product->value) ? $product->value : null }}">
	<label for="valor">Valor</label>
</div>
<div class="form-group">
    <div class="row">
        <label for="ativo">Ativo</label>
    </div>
    <div class="row">
      <input name="active" type="radio" id="ativo-s" value="Y" required="required" {{ isset($product->active) && $product->active == 'Y' ? ' checked="checked"' : null }} />
      <label for="ativo-s">Sim</label>
      <input name="active" type="radio" id="ativo-n" value="N" required="required" {{ isset($product->active) && $product->active == 'N' ? ' checked="checked"' : null }} />
      <label for="ativo-n">Não</label>
    </div>
</div>