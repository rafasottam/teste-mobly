@extends('layout')
@section('pagina_titulo', 'Carrinho de compras - Produtos')

@section('pagina_conteudo')
	<div class="container">
		<div class="row mt-4">
			<h3>Lista de produtos</h3>
		</div>
		<div class="row mt-3">
			<div class="card-deck">
				@foreach ($products as $product)
					<div class="card">
						<img class="card-img-top" src="{{ $product->image }}">
						<div class="card-body">
							<h5 class="card-title">{{ $product->id }} - {{ $product->name }}</h5>
							<h6 class="card-subtitle mb-2 text-muted">{{ $product->category }}</h6>
							<p class="card-text">{{ $product->description }}</p>
							<a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary">Editar</a>
							<a href="{{ route('products.destroy', $product->id) }}" class="btn btn-danger">Deletar</a>
						</div>
					</div>
				@endforeach
			</div>
		</div>
		<div class="row">
			<a href="{{ route('products.create') }}" class="btn btn-success mt-4">Novo Produto</a>
		</div>
	</div>

@endsection