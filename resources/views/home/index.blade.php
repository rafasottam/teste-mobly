@extends('layout')
@section('pagina_titulo', 'HOME')

@section('pagina_conteudo')

<div class="container">
	<div class="row">
		@foreach ($records as $record)
			<div class="card">
				<img class="card-img-top" src="{{ $record->image }}">
				<div class="card-body">
					<h5 class="card-title">{{ $record->id }} - {{ $record->name }}</h5>
					<h6 class="card-subtitle mb-2 text-muted">{{ $record->category }}</h6>
					<p class="card-text">{{ $record->description }}</p>
					<a href="{{ route('product', $record->id) }}" class="btn btn-primary">Detalhes</a>
				</div>
			</div>
		@endforeach
	</div>
</div>

@endsection