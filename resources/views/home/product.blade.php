@extends('layout')
@section('pagina_titulo', $record->name )

@section('pagina_conteudo')

<div class="container">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">{{ $record->name }}</h5>
            <p class="card-text">{{ $record->description }}</p>
            <h5 class="card-title">R$ {{ number_format($record->value, 2, ',', '.') }}</h5>
            <form method="POST" action="{{ route('car.add') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $record->id }}">
                    <button class="btn btn-success">Comprar</button>   
                </form>
        </div>
        <img class="card-img-bottom" src="{{ $record->image }}" alt="{{ $record->name }}" title="{{ $record->name }}">
    </div>
</div>

@endsection