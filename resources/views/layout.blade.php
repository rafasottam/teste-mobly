<!DOCTYPE html>
<html>
<head>
    <title>Loja virtual - @yield('pagina_titulo')</title>

    <link href="/css/app.css" rel="stylesheet">

</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand" href="{{ route('index') }}">Loja</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="{{ route('index') }} ">Home <span class="sr-only">(current)</span></a>
                    <a class="nav-item nav-link" href="{{ route('car.index') }}">Carrinho</a>
                    @if (Auth::guest())
                        <li><a class="nav-item nav-link" href="{{ url('/login') }}">Entrar</a></li>
                        <li><a class="nav-item nav-link" href="{{ url('/register') }}">Cadastre-se</a></li>
                    @else
                        <a class="nav-item nav-link" href="{{ route('products.index') }}">Produtos</a>
                        <li>
                            <a class="nav-item nav-link" href="{{ url('/logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                        </li>
                    @endif
                </div>
            </div>
        </nav>
    </header>
    <main>
        @yield('pagina_conteudo')

        @if(!Auth::guest())
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" class="hide">
                {{ csrf_field() }}
            </form>
        @endif
    </main>
    <footer class="footer bg-primary">
        <div class="mt-3 mb-3">© 2018 Copyright</div>
    </footer>
</body>
</html>