<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(); 

Route::get('/', 'HomeController@index')->name('index');
Route::get('/product/{id}', 'HomeController@product')->name('product');
Route::get('/car', 'CarController@index')->name('car.index');
Route::get('/car/add', function() {
    return redirect()->route('index');
});
Route::post('/car/add', 'CarController@add')->name('car.add');
Route::delete('/car/remove', 'CarController@remove')->name('car.remove');
Route::post('/car/conclude', 'CarController@conclude')->name('car.conclude');
Route::get('/car/purshase', 'CarController@purshase')->name('car.purshase');
Route::post('/car/cancel', 'CarController@cancel')->name('car.cancel');

Route::group(['prefix' => 'admin'], function () {
    Route::resource('products', 'Admin\ProductController');
});