# teste-mobly

Sistema de loja com carrinho de compras.

Utilizado em sua construção o hambiente de desenvolvimento Laragon com:

PHP -> 7.2.7
MySql -> 5.1.73
Laravel -> 5.6
Nginx -> 1.10.1

Necessária instalação do Composer.

Para execução do sistema, necessário criação de banco de dados mysql localmente. Estrutura de tabelas e dados para teste inclusos em arquivo sql no diretório.

Com o banco de dados criado e os arquivos no ambiente de desenvolvimento, abrir terminal e rodar o comando:
	php artisan serve

A resposta esperada é: Laravel development server started: <http://127.0.0.1:8000>
A partir dai o sistema já pode ser testado na URL informada acima.