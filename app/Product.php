<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image',
        'value',
        'character',
        'active'
    ];

    protected $dates = ['deleted_at'];

    public function category()
    {
        return $this->hasMany('App\CategoryProduct','product_id', 'id');
    }
}
