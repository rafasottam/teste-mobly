<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\CategoryProduct;
use File;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::with('category')->get();
        return view('admin.product.index', compact('products'));
    }

    public function create()
    {
        return view('admin.product.add');
    }

    public function store(Request $req)
    {
        $file = $req->file('image');
        $image = time() . $file->getClientOriginalName();
        $file->move('storage', $image);
        $url_image = 'http://'.$_SERVER['HTTP_HOST'] . '/storage/' . $image;
        
        Product::create([
            'image' => $url_image,
            'name' => $req->name,
            'description' => $req->description,
            'value' => $req->value,
            'active' => $req->active
        ]);
        
        return redirect()->route('admin.products');
    }
    
    public function edit($id)
    {
        $product = Product::find($id);
        return view('admin.product.edit', compact('product'));
    }
        
    public function update(Request $req, $id)
    {
        $products = $req->all();

        Produto::find($id)->update($products);

        return redirect()->route('admin.products');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('admin.products');
    }
}
