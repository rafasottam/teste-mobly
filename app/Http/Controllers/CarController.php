<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\Product;
use App\OrderProduct;

class CarController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $orders = Order::where([
            'status'  => 'RE',
            'user_id' => Auth::id()
            ])->get();

        return view('car.index', compact('orders'));
    }

    public function add()
    {

        $this->middleware('VerifyCsrfToken');

        $req = Request();
        $idproduct = $req->input('id');

        $product = Product::find($idproduct);
        if( empty($product->id) ) {
            $req->session()->flash('message-fail', 'Produto não encontrado!');
            return redirect()->route('car.index');
        }

        $iduser = Auth::id();

        $idorder = Order::consultId([
            'user_id' => $iduser,
            'status'  => 'RE'
            ]);

        if( empty($idorder) ) {
            $new_order = Order::create([
                'user_id' => $iduser,
                'status'  => 'RE'
                ]);

            $idorder = $new_order->id; 
        }

        OrderProduct::create([
            'order_id'  => $idorder,
            'product_id' => $idproduct,
            'value'      => $product->value,
            'status'     => 'RE'
            ]);

        $req->session()->flash('message-success', 'Produto adicionado com sucesso!');

        return redirect()->route('car.index');

    }

    public function remove()
    {

        $this->middleware('VerifyCsrfToken');
 
        $req = Request();
        $idorder           = $req->input('order_id');
        $idproduct          = $req->input('product_id');
        $remove_item = (boolean)$req->input('item');
        $iduser          = Auth::id();

        $idorder = Order::consultId([
            'id'      => $idorder,
            'user_id' => $iduser,
            'status'  => 'RE'
            ]);

        if( empty($idorder) ) {
            $req->session()->flash('message-fail', 'Pedido não encontrado!');
            return redirect()->route('car.index');
        }

        $where_product = [
            'order_id'  => $idorder,
            'product_id' => $idproduct
        ];

        $product = OrderProduct::where($where_product)->orderBy('id', 'desc')->first();
        if( empty($product->id) ) {
            $req->session()->flash('message-fail', 'Produto não encontrado!');
            return redirect()->route('car.index');
        }

        if( $remove_item ) {
            $where_product['id'] = $product->id;
        }
        OrderProduct::where($where_product)->delete();

        $check_order = OrderProduct::where([
            'order_id' => $product->order_id
            ])->exists();

        if( !$check_order ) {
            Order::where([
                'id' => $product->order_id
                ])->delete();
        }

        $req->session()->flash('message-sucesso', 'Produto removido com sucesso!');

        return redirect()->route('car.index');
    }

    public function conclude()
    {
        $this->middleware('VerifyCsrfToken');

        $req = Request();
        $idorder  = $req->input('order_id');
        $iduser = Auth::id();

        $check_order = Order::where([
            'id'      => $idorder,
            'user_id' => $iduser,
            'status'  => 'RE'
            ])->exists();

        if( !$check_order ) {
            $req->session()->flash('message-fail', 'Pedido não encontrado!');
            return redirect()->route('car.index');
        }

        $check_products = OrderProduct::where([
            'order_id' => $idorder
            ])->exists();
        if(!$check_products) {
            $req->session()->flash('message-fail', 'Produtos do pedido não encontrados!');
            return redirect()->route('car.index');
        }

        OrderProduct::where([
            'order_id' => $idorder
            ])->update([
                'status' => 'PA'
            ]);
        Order::where([
                'id' => $idorder
            ])->update([
                'status' => 'PA'
            ]);

        $req->session()->flash('message-sucesso', 'Compra concluída com sucesso!');

        return redirect()->route('car.purshase');
    }

    public function purchase()
    {

        $purchase = Order::where([
            'status'  => 'PA',
            'user_id' => Auth::id()
            ])->orderBy('created_at', 'desc')->get();

        $canceled = Order::where([
            'status'  => 'CA',
            'user_id' => Auth::id()
            ])->orderBy('updated_at', 'desc')->get();

        return view('car.purchase', compact('purchase', 'canceled'));

    }

    public function cancel()
    {
        $this->middleware('VerifyCsrfToken');

        $req = Request();
        $idorder       = $req->input('order_id');
        $idorder_prod = $req->input('id');
        $iduser      = Auth::id();

        if( empty($idorder_prod) ) {
            $req->session()->flash('message-fail', 'Nenhum item selecionado para cancelamento!');
            return redirect()->route('car.compras');
        }

        $check_order = Order::where([
            'id'      => $idorder,
            'user_id' => $iduser,
            'status'  => 'PA'
            ])->exists();

        if( !$check_order ) {
            $req->session()->flash('message-fail', 'Pedido não encontrado para cancelamento!');
            return redirect()->route('car.compras');
        }

        $check_products = OrderProduct::where([
                'order_id' => $idorder,
                'status'    => 'PA'
            ])->whereIn('id', $idorder_prod)->exists();

        if( !$check_products ) {
            $req->session()->flash('message-fail', 'Produtos do pedido não encontrados!');
            return redirect()->route('car.compras');
        }

        OrderProduct::where([
                'order_id' => $idorder,
                'status'    => 'PA'
            ])->whereIn('id', $idorder_prod)->update([
                'status' => 'CA'
            ]);

        $check_pedido_cancel = OrderProduct::where([
                'order_id' => $idorder,
                'status'    => 'PA'
            ])->exists();

        if( !$check_pedido_cancel ) {
            Order::where([
                'id' => $idorder
            ])->update([
                'status' => 'CA'
            ]);

            $req->session()->flash('message-sucesso', 'Compra cancelada com sucesso!');

        } else {
            $req->session()->flash('message-sucesso', 'Item(ns) da compra cancelado(s) com sucesso!');
        }

        return redirect()->route('car.compras');
    }
}
