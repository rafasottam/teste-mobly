<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class HomeController extends Controller
{
    public function index()
    {
        $records = Product::where([
            'active' => 'Y'
            ])->get();

        return view('home.index', compact('records'));
    }

    public function product($id = null)
    {
        if( !empty($id) ) {
            $record = Product::where([
                'id'    => $id,
                'active' => 'Y'
                ])->first();

            if( !empty($record) ) {
                return view('home.product', compact('record'));
            }
        }
        return redirect()->route('index');
    }
}
