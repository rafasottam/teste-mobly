<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderProduct extends Model
{
    protected $fillable = [
        'order_id',
        'product_id',
        'status',
        'value'
    ];
    
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }

    protected $dates = ['deleted_at'];
}
