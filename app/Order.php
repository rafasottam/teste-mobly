<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'status'
    ];

    protected $dates = ['deleted_at'];

    public function order_product()
    {
        return $this->hasMany('App\OrderProduct')
            ->select( \DB::raw('product_id, sum(value) as value, count(1) as qtd') )
            ->groupBy('product_id')
            ->orderBy('product_id', 'desc');
    }

    public function order_products_itens()
    {
        return $this->hasMany('App\OrderProduct');
    }

    public static function consultId($where)
    {
        $order = self::where($where)->first(['id']);
        return !empty($order->id) ? $order->id : null;
    }
}
