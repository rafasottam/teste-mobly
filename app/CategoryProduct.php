<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryProduct extends Model
{
    protected $fillable = [
        'category',
        'product_id'
    ];
    
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }

    protected $dates = ['deleted_at'];
}
